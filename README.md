# Installer et configurer l'application
L'application SortirEtienne est conçue pour fonctionner dans un environnement WampServer.
Ce Readme décrit les actions à suivre pour exécuter l'application de cet environnement.

## Prérequis
 - SGBD : MySQL
 - User : root (no password)
 - BDD : tp-sortir
 - WampServer lancé et tous les services au vert

## Installation
1. Positionnez-vous dans le répertoire "www" de WampServer
2. Clonez ce projet `git clone https://gitlab.com/et.cassin/eni-sortir.git`
3. Positionnez-vous dans le répertoire ENI-Sortir ainsi récupéré `cd eni-sortir`
---
4. Exécutez le script d'initialisation `sh init.sh`  

OU  


4. Installez les dépendances du projet `composer install`
5. Créez le schéma de la BDD `php bin/console doctrine:schema:update --force`
6. Ajouter des données de test `php bin/console doctrine:fixtures:load`
---
7. Accédez à l'url http://localhost/SortirEtienne/public/sortie/
