let select;
let checkbox_master;
let checkboxes;
let btn_submit;

onload += init;

function init() {
    select = document.getElementById('multiple_action_selector');
    checkbox_master = document.getElementById('check_all');
    checkboxes = document.getElementsByClassName('cb_select');
    btn_submit = document.getElementById('submit_action_multiple');

    checkbox_master.addEventListener('change', checkAll);
    btn_submit.addEventListener('click', envoyer);
}

function checkAll() {
    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = checkbox_master.checked;
    }
}

function envoyer() {
    let ids = [...checkboxes].filter(cb => cb.checked).map(cb => parseInt(cb.id.substring(7)));
    let action = select.value;

    if (action) {
        let xhr = new XMLHttpRequest();

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                location.reload();
            }
        }

        xhr.open(action==='disable'?"PUT":"DELETE", "http://localhost/SortirEtienne/public/user/"+action);
        xhr.send(JSON.stringify(ids));
    }
}