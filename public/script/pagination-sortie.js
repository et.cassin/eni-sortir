const RESULT_PER_PAGE = 20;
let index = 0
table_sorties = document.getElementsByClassName('table-row');
let nb_pages = Math.trunc((table_sorties.length - 1) / RESULT_PER_PAGE) + 1;
let pagination_holder = document.getElementById('pagination');

generateArrows(table_sorties.length);
updatePagination();
function generateArrows(table_size) {
    nb_pages = Math.trunc((table_size - 1) / RESULT_PER_PAGE) + 1;
    pagination_holder.innerHTML = '';
    pagination_holder.innerHTML += '<p onclick="setIndex(0)">&lt;&lt;</p>';
    pagination_holder.innerHTML += '<p onclick="setIndex(-1, false)">&lt;</p>';
    for (let i = 0; i < nb_pages; i++) {
        pagination_holder.innerHTML += '<p onclick="setIndex('+i+')">'+(i+1)+'</p>';
    }
    pagination_holder.innerHTML += '<p onclick="setIndex(1, false)">&gt;</p>';
    pagination_holder.innerHTML += '<p onclick="setIndex('+(nb_pages-1)+')">&gt;&gt;</p>';
}

function setIndex(i, override = true) {
    index = override ? i : index + i;
    if (index < 0) index = 0;
    if (index >= nb_pages) index = nb_pages - 1;
    updateTable();
}

function updatePagination() {
    let shouldShow;
    let visibleRowsIndex = 0;

    for (let i = 0; i < table_sorties.length; i++) {
        let row = table_sorties[i];

        if (row.style.display === 'none') {
            continue;
        }

        shouldShow = visibleRowsIndex >= index * RESULT_PER_PAGE && visibleRowsIndex < (index+1) * RESULT_PER_PAGE;

        if (shouldShow) {
            row.style.display = 'table-row';
            row.style.backgroundColor = (visibleRowsIndex % 2 === 1) ? 'lightgrey' : 'white';
        } else {
            row.style.display = 'none';
        }

        visibleRowsIndex++;
    }
}
