let btn_annuler;
let form_annuler;
let btn_close;

onload = init;

function init() {
    btn_annuler = document.getElementById('btn_annuler');
    btn_close = document.getElementById('close');
    form_annuler = document.getElementById('form_suppression');

    btn_annuler.addEventListener('click', displayForm);
    btn_close.addEventListener('click', closeForm);
}

function displayForm() {
    form_annuler.style.display = 'flex';
}

function closeForm() {
    form_annuler.style.display = 'none';
}