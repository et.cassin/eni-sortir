let filtres;
let table_sorties;
let info_filtre;
let user;
let btn_reset;

let apply_contient = false;
let apply_after = false;
let apply_etat = false;
let apply_before = false;
let apply_inscrit = false;
let apply_noninscrit = false;
let apply_organisateur = false;
let apply_passees = false;
let apply_site = false;

onload = init;

function init() {
    filtres = document.getElementsByClassName('filter-elem');
    table_sorties = document.getElementsByClassName('table-row');
    user = document.getElementById('user').value;
    info_filtre = document.getElementById("info-filtre")
    btn_reset = document.getElementById("reset");

    filtres.contient.addEventListener("input", () => {apply_contient = filtres.contient.value.length > 0;setIndex(0);});
    filtres.after.addEventListener("change", () => {apply_after = filtres.after.value.length > 0;setIndex(0);});
    filtres.before.addEventListener("change", () => {apply_before = filtres.before.value.length > 0;setIndex(0);});
    filtres.inscrit.addEventListener("change", () => {apply_inscrit = !apply_inscrit;setIndex(0);});
    filtres.noninscrit.addEventListener("change", () => {apply_noninscrit = !apply_noninscrit;setIndex(0);});
    filtres.organisateur.addEventListener("change", () => {apply_organisateur = !apply_organisateur;setIndex(0);});
    filtres.passees.addEventListener("change", () => {apply_passees = !apply_passees;setIndex(0);});
    filtres.site.addEventListener("change", () => {apply_site = filtres.site.value.length > 0;setIndex(0);});
    filtres.etat.addEventListener("change", () => {apply_etat = filtres.etat.value.length > 0;setIndex(0);});
    btn_reset.addEventListener("click", reset);
}

function updateTable() {
    let shown = 0;

    for (let i = 0; i < table_sorties.length; i++) {
        let row = table_sorties[i];
        let cells = row.cells;

        let attr_sortie = {
                'nom' : cells[0].innerText,
                'site' : cells[1].innerText,
                'date' : splitStringToDate(cells[2].innerText),
                'inscription' : cells[3].innerText,
                'etat' : cells[5].innerText,
                'inscrit' : cells[6].innerText,
                'organisateur' : cells[7].innerText
        };

        let shouldShow = true;

        if (apply_site) {
            if (filtres.site.value !== attr_sortie.site) {
                shouldShow = false;
            }
        }

        if (apply_etat) {
            if (filtres.etat.value !== attr_sortie.etat) {
                shouldShow = false;
            }
        }

        if (apply_contient) {
            if (!attr_sortie.nom.toLowerCase().includes(filtres.contient.value.toLowerCase())) {
                shouldShow = false;
            }
        }

        if (apply_after) {
            let filtre_date = new Date(filtres.after.value);
            if (filtre_date >= attr_sortie.date) {
                shouldShow = false;
            }
        }

        if (apply_before) {
            let filtre_date = new Date(filtres.before.value);
            if (filtre_date <= attr_sortie.date) {
                shouldShow = false;
            }
        }

        if (apply_organisateur) {
            if (user !== attr_sortie.organisateur) {
                shouldShow = false;
            }
        }

        if (apply_inscrit) {
            if (!attr_sortie.inscrit.includes('X')) {
                shouldShow = false;
            }
        }

        if (apply_noninscrit) {
            if (attr_sortie.inscrit.includes('X')) {
                shouldShow = false;
            }
        }

        if (apply_passees) {
            if (attr_sortie.etat !== "Passée") {
                shouldShow = false;
            }
        }

        if (shouldShow) {
            row.style.display = 'table-row';
            row.style.backgroundColor = (shown % 2 === 1) ? 'lightgrey' : 'white';
            shown++;
        } else {
            row.style.display = 'none';
        }
    }

    if (shown === 0) {
        info_filtre.innerHTML = 'Aucun résultat ne correspond à votre recherche.';
    } else {
        generateArrows(shown);
        updatePagination();
        info_filtre.innerHTML = '';
    }
}

function reset() {
    apply_contient = false;
    apply_after = false;
    apply_before = false;
    apply_inscrit = false;
    apply_etat = false;
    apply_noninscrit = false;
    apply_organisateur = false;
    apply_passees = false;
    apply_site = false;

    updateTable();
}

function splitStringToDate(date) {
    let parts = date.split(" ");
    let date_parts = parts[0].split("/");
    let hour_parts = parts[1].split(":");
    return new Date(+date_parts[2], date_parts[1]-1, +date_parts[0], +hour_parts[0], +hour_parts[1]);
}