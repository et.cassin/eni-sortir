let btn_ok;
let div_trop_style;

btn_ok = document.getElementById('btn_ok');
div_trop_style = document.getElementById('trop-style');

btn_ok.addEventListener('click', hide);

function hide() {
    div_trop_style.style.opacity = "0";
    div_trop_style.style.borderRadius = "50%";
    div_trop_style.style.width = "0";
    div_trop_style.style.height = "0";

    setTimeout(() => {
        div_trop_style.innerHTML = '';
    }, 380);
}