const RESULT_PER_PAGE = 20;
let index = 0
table_sorties = document.getElementsByClassName('table-row');
let nb_pages = Math.trunc((table_sorties.length - 1) / RESULT_PER_PAGE) + 1;

generateArrows();
updatePagination();
function generateArrows() {
    let pagination_holder = document.getElementById('pagination');
    pagination_holder.innerHTML = '';
    pagination_holder.innerHTML += '<p onclick="setIndex(0)">&lt;&lt;</p>';
    pagination_holder.innerHTML += '<p onclick="setIndex(-1, false)">&lt;</p>';
    for (let i = 0; i < nb_pages; i++) {
        pagination_holder.innerHTML += '<p onclick="setIndex('+i+')">'+(i+1)+'</p>';
    }
    pagination_holder.innerHTML += '<p onclick="setIndex(1, false)">&gt;</p>';
    pagination_holder.innerHTML += '<p onclick="setIndex('+(nb_pages-1)+')">&gt;&gt;</p>';
}

function setIndex(i, override = true) {
    index = override ? i : index + i;
    if (index < 0) index = 0;
    if (index >= nb_pages) index = nb_pages - 1;
    updatePagination();
}

function updatePagination() {
    let shouldShow;

    for (let i = 0; i < table_sorties.length; i++) {
        let row = table_sorties[i];

        shouldShow = i >= index * RESULT_PER_PAGE && i < (index+1) * RESULT_PER_PAGE;

        if (shouldShow) {
            row.style.display = 'table-row';
            row.style.backgroundColor = (i % 2 === 1) ? 'lightgrey' : 'white';
        } else {
            row.style.display = 'none';
        }
    }
}
