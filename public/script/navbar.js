let btn_burger;
let navbar_navigation;

btn_burger = document.getElementById('burger');
navbar_navigation = document.getElementById('navigation');

btn_burger.addEventListener('click', swapNavbar);

function swapNavbar() {
    if (navbar_navigation.style.display === 'flex') {
        navbar_navigation.style.display = 'none';
    } else {
        navbar_navigation.style.display = 'flex';
    }
}