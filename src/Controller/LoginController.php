<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\String\ByteString;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'app_login')]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_sortie_index');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login/index.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/forgotten', name: 'app_forgotten', methods: ['GET', 'POST'])]
    public function forgotten(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_sortie_index');
        }

        $mail = '';
        $random = '';
        if ($request->request->has('_username')) {
            $mail = $request->request->get('_username');
            $user = $userRepository->findOneBy(['email' => $mail]);

            $random = ByteString::fromRandom(10)->toString();
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $random
                )
            );

            $userRepository->save($user, true);
        }

        return $this->render('login/forgotten.html.twig', [
            'mail' => $mail,
            'random' => $random
        ]);
    }
}