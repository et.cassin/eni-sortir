<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/user')]
class UserController extends AbstractController
{
    #[Route('/', name: 'app_user_index', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'La consultation des utilisateurs est réservée au administrateurs !');

        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/disable', name: 'app_disable', methods: ['PUT'])]
    public function disable(Request $request, UserRepository $userRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'La consultation des utilisateurs est réservée au administrateurs !');

        $userRepository->disableByIds(json_decode($request->getContent()));
        return $this->json('');
    }

    #[Route('/delete', name: 'app_delete', methods: ['DELETE'])]
    public function deleteAll(Request $request, UserRepository $userRepository): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'La consultation des utilisateurs est réservée au administrateurs !');

        $userRepository->deleteByIds(json_decode($request->getContent()));
        return $this->json('');
    }

    #[Route('/{id}', name: 'app_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        if ($this->getUser()->getId() == $user->getId()) {
            return $this->redirectToRoute('app_user_edit', ['id' => $user->getId()]);
        }

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserRepository $userRepository, UserPasswordHasherInterface $userPasswordHasher, SluggerInterface $slugger): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }
        if ($this->getUser()->getId() != $user->getId()) {
            $this->denyAccessUnlessGranted('ROLE_ADMIN', null, "L'édition d'un profil est réservé à son propriétaire !");
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $errors = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $pass = $form->get('password')->getData();
            $confirm = $form->get('confirmation')->getData();
            $image = $form->get('image')->getData();

            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    dd($e);
                }
                $user->setImage($newFilename);
            }

            if (!empty($pass)) {
                if ($pass === $confirm) {
                    $user->setPassword(
                        $userPasswordHasher->hashPassword(
                            $user,
                            $pass
                        )
                    );
                } else {
                    $errors[] = 'Les mots de passe saisis sont différents.';
                }
            }
            if (count($errors) == 0) {
                $userRepository->save($user, true);
            }
        }
        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
            'errors' => $errors
        ]);
    }

    #[Route('/{id}', name: 'app_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
