<?php

namespace App\Controller;

use App\Entity\Site;
use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            if ($form->get('admin')->getData()) {
                $user->setRoles(['ROLE_ADMIN']);
            } else {
                $user->setRoles(['ROLE_USER']);
            }

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_user_index');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/registerAll', name: 'app_register_multiple_users')]
    public function registerMultiple(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $content = file_get_contents($request->files->get('csv_users'));
        $content_array = preg_split("/\r\n|\n|\r/", $content);

        foreach ($content_array as $line) {
            $datas = explode(";", $line);

            $user = new User();
            $user->setEmail($datas[0]);
            $user->setNom($datas[2]);
            $user->setPrenom($datas[3]);
            $user->setTelephone($datas[4]);
            $user->setPseudo($datas[5]);

            $user->setActif(true);
            $user->setSite($entityManager->find(Site::class, 1));
            $user->setRoles(["ROLE_USER"]);

            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $datas[1]
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_user_index');
    }
}
