<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Form\SortieType;
use App\Repository\EtatRepository;
use App\Repository\SiteRepository;
use App\Repository\SortieRepository;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/sortie')]
class SortieController extends AbstractController
{
    /**
     * @throws Exception
     */
    #[Route('/', name: 'app_sortie_index', methods: ['GET'])]
    public function index(SortieRepository $sortieRepository, SiteRepository $siteRepository, EtatRepository $etatRepository): Response
    {
        $this->updateSortieEtat($sortieRepository);

        setcookie("first_time", true);
        return $this->render('sortie/index.html.twig', [
            'sorties' => $sortieRepository->findAllRecent(),
            'sites' => $siteRepository->findAll()
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/new', name: 'app_sortie_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SortieRepository $sortieRepository, EtatRepository $etatRepository, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $sortie = new Sortie();
        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);
        $sortie->setSite($user->getSite());
        $form = $this->createForm(SortieType::class, $sortie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sortie->setEtat($etatRepository->findOneBy(['libelle' => 'Créée']));
            $sortie->setOrganisateur($this->getUser());
            $sortieRepository->save($sortie, true);

            $this->updateSortieEtat($sortieRepository);
            return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sortie/new.html.twig', [
            'sortie' => $sortie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sortie_show', methods: ['GET'])]
    public function show(Sortie $sortie, EtatRepository $etatRepository): Response
    {
        return $this->render('sortie/show.html.twig', [
            'sortie' => $sortie,
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}/publish', name: 'app_sortie_publish', methods: ['GET'])]
    public function publish(Sortie $sortie, SortieRepository $sortieRepository, EtatRepository $etatRepository): Response
    {
        if (!$this->getUser() || $this->getUser()->getId() !== $sortie->getOrganisateur()->getId()) {
            return $this->redirectToRoute('app_login');
        }

        $published_state = $etatRepository->findOneBy(['libelle' => 'Ouverte']);
        $sortie->setEtat($published_state);
        $sortieRepository->save($sortie, true);
        $this->updateSortieEtat($sortieRepository);
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}/inscrire', name: 'app_sortie_inscrire', methods: ['GET'])]
    public function inscrire(Sortie $sortie, SortieRepository $sortieRepository): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $sortie->addInscrit($this->getUser());
        $sortieRepository->save($sortie, true);
        $this->updateSortieEtat($sortieRepository);
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}/desinscrire', name: 'app_sortie_desinscrire', methods: ['GET'])]
    public function desinscrire(Sortie $sortie, SortieRepository $sortieRepository): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $sortie->removeInscrit($this->getUser());
        $sortieRepository->save($sortie, true);
        $this->updateSortieEtat($sortieRepository);
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}/annuler', name: 'app_sortie_annuler', methods: ['POST'])]
    public function annuler(Request $request, Sortie $sortie, SortieRepository $sortieRepository, EtatRepository $etatRepository): Response
    {
        if (!$this->getUser() || $this->getUser()->getId() !== $sortie->getOrganisateur()->getId()) {
            return $this->redirectToRoute('app_login');
        }

        $cancelledstate = $etatRepository->findOneBy(['libelle' => 'Annulée']);
        $sortie->setMotif($request->request->get('motif'));
        $sortie->setEtat($cancelledstate);

        $sortieRepository->save($sortie, true);

        $this->updateSortieEtat($sortieRepository);
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}/edit', name: 'app_sortie_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Sortie $sortie, SortieRepository $sortieRepository): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(SortieType::class, $sortie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sortieRepository->save($sortie, true);

            $this->updateSortieEtat($sortieRepository);
            return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sortie/edit.html.twig', [
            'sortie' => $sortie,
            'form' => $form,
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/{id}', name: 'app_sortie_delete', methods: ['POST'])]
    public function delete(Request $request, Sortie $sortie, SortieRepository $sortieRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sortie->getId(), $request->request->get('_token'))) {
            $sortieRepository->remove($sortie, true);
        }

        $this->updateSortieEtat($sortieRepository);
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws Exception
     */
    private function updateSortieEtat(SortieRepository $sortieRepository): void
    {
        $sorties = $sortieRepository->findAll();
        $now = new DateTime();

        $sortiesToClose = [];
        $sortiesToCurrently = [];
        $sortiesToPass = [];
        $sortiesToReopen = [];
        foreach ($sorties as $sortie) {
            if ($sortie->getEtat()->getLibelle() === 'Passée' || $sortie->getEtat()->getLibelle() === 'Annulée') {
                continue;
            }

            $dateFin = clone $sortie->getDateDebut();
            $dateFin->add(new DateInterval('PT'.$sortie->getDuree().'M'));

            if ($now > $dateFin) {
                $sortiesToPass[] = $sortie->getId();
            } elseif ($dateFin > $now and $now > $sortie->getDateDebut()) {
                $sortiesToCurrently[] = $sortie->getId();
            } elseif ($sortie->getDateLimiteInscription() < $now || count($sortie->getInscrits()) === $sortie->getNbInscriptionsMax()) {
                $sortiesToClose[] = $sortie->getId();
            }

            if ($sortie->getEtat()->getLibelle() === 'Cloturée' && $sortie->getDateLimiteInscription() > $now && count($sortie->getInscrits()) < $sortie->getNbInscriptionsMax()) {
                $sortiesToReopen[] = $sortie->getId();
            }
        }
        if (!empty($sortiesToClose)) {
            $sortieRepository->close($sortiesToClose);
        }
        if (!empty($sortiesToCurrently)) {
            $sortieRepository->current($sortiesToCurrently);
        }
        if (!empty($sortiesToPass)) {
            $sortieRepository->pass($sortiesToPass);
        }
        if (!empty($sortiesToReopen)) {
            $sortieRepository->reopen($sortiesToReopen);
        }
    }
}
