<?php

namespace App\DataFixtures;

use App\Entity\Etat;
use App\Entity\Lieu;
use App\Entity\Site;
use App\Entity\Sortie;
use App\Entity\User;
use App\Entity\Ville;
use DateInterval;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    const NB_PARTICIPANTS = 50;
    const NB_SITES = 50;
    const NB_VILLES = 40;
    const NB_LIEUX = 60;
    const NB_SORTIES = 250;

    private array $etats_libelles = ['Créée', 'Ouverte', 'Cloturée', 'En cours', 'Passée', 'Annulée'];
    private array $etats = [];
    private array $villes = [];
    private array $lieux = [];
    private array $sites = [];
    private array $participants = [];

    public function __construct(private UserPasswordHasherInterface $userPasswordHasher)
    {

    }

    public function load(ObjectManager $manager): void
    {
        $this->loadEtat($manager);
        $this->loadVille($manager);
        $this->loadLieu($manager);
        $this->loadSite($manager);
        $this->loadParticipant($manager);
        $this->loadSortie($manager);

        $manager->flush();
    }

    private function loadEtat(ObjectManager $manager) {
        foreach ($this->etats_libelles as $etat) {
            $e = new Etat();
            $e->setLibelle($etat);

            $this->etats[] = $e;
            $manager->persist($e);
        }
    }

    private function loadVille(ObjectManager $manager) {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_VILLES; $i++) {
            $ville = new Ville();
            $ville->setNom($faker->city);
            $ville->setCodePostal($faker->postcode);

            $this->villes[] = $ville;
            $manager->persist($ville);
        }
    }

    private function loadLieu(ObjectManager $manager) {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_LIEUX; $i++) {
            $lieu = new Lieu();
            $lieu->setNom($faker->word);
            $lieu->setRue($faker->address);
            $lieu->setLatitude($faker->latitude);
            $lieu->setLongitude($faker->longitude);
            $lieu->setVille($this->villes[mt_rand(0, self::NB_VILLES -1)]);

            $this->lieux[] = $lieu;
            $manager->persist($lieu);
        }
    }

    private function loadSite(ObjectManager $manager) {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_SITES; $i++) {
            $site = new Site();
            $site->setNom($faker->name);

            $this->sites[] = $site;
            $manager->persist($site);
        }
    }

    private function loadParticipant(ObjectManager $manager) {
        $faker = Factory::create('fr_FR');

        $this->loadFixedUsers($manager);

        for ($i = 0; $i < self::NB_PARTICIPANTS; $i++) {
            $participant = new User();
            $participant->setNom($faker->lastName);
            $participant->setPrenom($faker->firstName);
            $participant->setPseudo($faker->word.$i);

            $participant->setPassword(
                $this->userPasswordHasher->hashPassword(
                    $participant,
                    'password'
                )
            );
            $participant->setTelephone($faker->phoneNumber);
            $participant->setEmail($faker->email);
            $participant->setRoles(mt_rand(0, 1) ? ['ROLE_ADMIN'] : ['ROLE_USER']);
            $participant->setActif(true);
            $participant->setSite($this->sites[mt_rand(0, self::NB_SITES-1)]);

            $this->participants[] = $participant;
            $manager->persist($participant);
        }
    }

    private function loadFixedUsers(ObjectManager $manager) {
        $participant = new User();
        $participant->setNom('Skywalker');
        $participant->setPrenom('Anakin');
        $participant->setPseudo('Ani');

        $participant->setPassword(
            $this->userPasswordHasher->hashPassword(
                $participant,
                'password'
            )
        );
        $participant->setTelephone('0000000000');
        $participant->setEmail('ani@darkside.com');
        $participant->setRoles(['ROLE_USER']);
        $participant->setActif(true);
        $participant->setSite($this->sites[mt_rand(0, self::NB_SITES-1)]);

        $this->participants[] = $participant;
        $manager->persist($participant);

        $participant = new User();
        $participant->setNom('Jinn');
        $participant->setPrenom('Qui gon');
        $participant->setPseudo('quigon');

        $participant->setPassword(
            $this->userPasswordHasher->hashPassword(
                $participant,
                'password'
            )
        );
        $participant->setTelephone('0101010101');
        $participant->setEmail('quigonn@jedi-master.com');
        $participant->setRoles(['ROLE_ADMIN']);
        $participant->setActif(true);
        $participant->setSite($this->sites[mt_rand(0, self::NB_SITES-1)]);

        $this->participants[] = $participant;
        $manager->persist($participant);
    }

    /**
     * @throws Exception
     */
    private function loadSortie(ObjectManager $manager) {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_SORTIES; $i++) {
            $sortie = new Sortie();
            $sortie->setNom($faker->name);
            $sortie->setDateDebut($faker->dateTimeBetween('-40 days', '+40 days'));
            $sortie->setDuree(mt_rand(5, 240));
            $sortie->setDateLimiteInscription($sortie->getDateDebut());
            $sortie->setNbInscriptionsMax(mt_rand(2, 40));
            $sortie->setInfosSortie($faker->realText());
            $sortie->setEtat($this->etats[0]);
            $sortie->setLieu($this->lieux[mt_rand(0, self::NB_LIEUX -1)]);
            $orga = $this->participants[mt_rand(0, self::NB_PARTICIPANTS -1)];
            $sortie->setOrganisateur($orga);
            $sortie->setSite($orga->getSite());

            $manager->persist($sortie);
        }
    }
}
