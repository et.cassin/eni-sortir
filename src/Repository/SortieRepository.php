<?php

namespace App\Repository;

use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Sortie>
 *
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    public function findAllRecent() {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT s FROM App\Entity\Sortie s WHERE s.dateDebut > DATE_SUB(CURRENT_DATE(), 1, 'MONTH') ");
        return $query->getResult();
    }

    public function save(Sortie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Sortie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function close(array $sorties): void
    {
        $em = $this->getEntityManager();
        $dql = "UPDATE App\Entity\Sortie s 
                    SET s.etat = (SELECT e FROM App\Entity\Etat e WHERE e.libelle = 'Cloturée') 
                    WHERE s.id IN (:ids)";
        $query = $em->createQuery($dql);
        $query->setParameter('ids', $sorties);
        $query->execute();
        $em->flush();
    }

    public function current(array $sorties): void
    {
        $em = $this->getEntityManager();
        $dql = "UPDATE App\Entity\Sortie s 
                    SET s.etat = (SELECT e FROM App\Entity\Etat e WHERE e.libelle = 'En cours') 
                    WHERE s.id IN (:ids)";
        $query = $em->createQuery($dql);
        $query->setParameter('ids', $sorties);
        $query->execute();
        $em->flush();
    }

    public function pass(array $sorties): void
    {
        $em = $this->getEntityManager();
        $dql = "UPDATE App\Entity\Sortie s 
                    SET s.etat = (SELECT e FROM App\Entity\Etat e WHERE e.libelle = 'Passée') 
                    WHERE s.id IN (:ids)";
        $query = $em->createQuery($dql);
        $query->setParameter('ids', $sorties);
        $query->execute();
        $em->flush();
    }

    public function reopen(array $sorties): void
    {
        $em = $this->getEntityManager();
        $dql = "UPDATE App\Entity\Sortie s 
                    SET s.etat = (SELECT e FROM App\Entity\Etat e WHERE e.libelle = 'Ouverte') 
                    WHERE s.id IN (:ids)";
        $query = $em->createQuery($dql);
        $query->setParameter('ids', $sorties);
        $query->execute();
        $em->flush();
    }

//    public function findOneBySomeField($value): ?Sortie
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
